





## 1. Data origin/preparation

The following evaluations are based on the OpenAPC core data file (apc_de.csv) and invoice data for Germany kindly provided by Frontiers (covering the time frame from 2014 to 2018).

As a first step, all articles in the Frontiers data set were preprocessed:

1) Author affiliations were mapped to institutional designations based on OpenAPC standards. It should be noted that this process was not exhaustive: Institutions which are most likely not relevant to OpenAPC (such as small private companies or medical facilities which aren't academic teaching hospitals) were exluded, this also applied to organisations appearing only once or twice.  As a consequence, only 5649 out of the original 6029 articles were taking part in the evaluation.
2) If applicable, organisational indicators were added. Possible values were MPG (Max Planck Society), WGL (Leibniz Association), HGF (Helmholtz Association of German Research Centres), FHG (Fraunhofer Society) or Federal (German federal agency). 
3) Articles were looked up in the OpenAPC data set (via DOI matching).

## 2. Evaluation

### 2.1 Institutions not participating in OpenAPC

The following table lists all institutions not participating in OpenAPC, sorted by Frontiers article output:


|institution                |full name                                                                              |organisation | article count|
|:--------------------------|:--------------------------------------------------------------------------------------|:------------|-------------:|
|Charité                    |-                                                                                      |-            |           156|
|Hamburg U                  |-                                                                                      |-            |           116|
|Frankfurt U                |-                                                                                      |-            |           108|
|MH Hannover                |-                                                                                      |-            |            85|
|Jena U                     |-                                                                                      |-            |            83|
|Marburg U                  |-                                                                                      |-            |            77|
|RWTH                       |-                                                                                      |-            |            73|
|Duesseldorf U              |-                                                                                      |-            |            69|
|Koeln U                    |-                                                                                      |-            |            65|
|Bonn U                     |-                                                                                      |-            |            60|
|Magdeburg U                |-                                                                                      |-            |            57|
|Kiel U                     |-                                                                                      |-            |            56|
|Saarland U                 |-                                                                                      |-            |            54|
|Lübeck U                   |-                                                                                      |-            |            47|
|HU Berlin                  |-                                                                                      |-            |            45|
|Rostock U                  |-                                                                                      |-            |            44|
|IPK                        |Leibniz-Institut für Pflanzengenetik und Kulturpflanzenforschung                       |WGL          |            37|
|HZI                        |Helmholtz-Zentrum für Infektionsforschung                                              |HGF          |            36|
|Hohenheim U                |-                                                                                      |-            |            35|
|DKFZ                       |Deutsches Krebsforschungszentrum                                                       |HGF          |            34|
|DZNE                       |Deutsches Zentrum für Neurodegenerative Erkrankungen                                   |HGF          |            31|
|HMGU                       |Helmholtz Zentrum München – Deutsches Forschungszentrum für Gesundheit und Umwelt      |HGF          |            31|
|GEOMAR                     |GEOMAR Helmholtz-Zentrum für Ozeanforschung Kiel                                       |HGF          |            28|
|Greifswald U               |-                                                                                      |-            |            28|
|DSHS                       |Deutsche Sporthochschule Köln                                                          |-            |            26|
|IWM                        |Leibniz-Institut für Wissensmedien                                                     |WGL          |            24|
|HKI                        |Leibniz-Institut für Naturstoff-Forschung und Infektionsbiologie – Hans-Knöll-Institut |WGL          |            23|
|ZMT                        |Leibniz-Zentrum für Marine Tropenforschung                                             |WGL          |            23|
|IGZ                        |Leibniz-Institut für Gemüse- und Zierpflanzenbau                                       |WGL          |            21|
|TU Kaiserslautern          |-                                                                                      |-            |            21|
|AWI                        |Alfred-Wegener-Institut Helmholtz-Zentrum für Polar- und Meeresforschung               |HGF          |            20|
|DLR                        |Deutsches Zentrum für Luft- und Raumfahrt                                              |HGF          |            18|
|IOW                        |Leibniz-Institut für Ostseeforschung                                                   |WGL          |            18|
|LIN                        |Leibniz-Institut für Neurobiologie                                                     |WGL          |            18|
|RKI                        |Robert Koch-Institut                                                                   |Federal      |            17|
|Bremen Jacobs              |-                                                                                      |-            |            15|
|JKI                        |Julius-Kühn-Institut – Bundesforschungsinstitut für Kulturpflanzen                     |Federal      |            13|
|DSMZ                       |Leibniz-Institut DSMZ – Deutsche Sammlung von Mikroorganismen und Zellkulturen         |WGL          |            12|
|FBN                        |Leibniz-Institut für Nutztierbiologie                                                  |WGL          |            12|
|HMTMH                      |Hochschule für Musik, Theater und Medien Hannover                                      |-            |            11|
|IfADo                      |Leibniz-Institut für Arbeitsforschung                                                  |WGL          |            11|
|Lüneburg U                 |-                                                                                      |-            |            10|
|ZI                         |-                                                                                      |-            |            10|
|FZB                        |Forschungszentrum Borstel – Leibniz-Lungenzentrum                                      |WGL          |             9|
|Koblenz U                  |-                                                                                      |-            |             9|
|Paderborn U                |-                                                                                      |-            |             9|
|UKSH                       |Universitätsklinikum Schleswig-Holstein                                                |-            |             9|
|ZALF                       |Leibniz-Zentrum für Agrarlandschaftsforschung                                          |WGL          |             9|
|BfR                        |Bundesinstitut für Risikobewertung                                                     |Federal      |             8|
|FIAS                       |-                                                                                      |-            |             8|
|GSI                        |GSI Helmholtzzentrum für Schwerionenforschung                                          |HGF          |             8|
|IME                        |Fraunhofer-Institut für Molekularbiologie und Angewandte Oekologie                     |FHG          |             8|
|Mannheim U                 |-                                                                                      |-            |             8|
|Vivantes                   |Vivantes – Netzwerk für Gesundheit GmbH                                                |-            |             8|
|Bayer AG                   |-                                                                                      |-            |             7|
|FLI                        |-                                                                                      |-            |             7|
|IMB                        |Institut für Molekulare Biologie                                                       |-            |             7|
|DPZ                        |Deutsche Primatenzentrum                                                               |WGL          |             6|
|DRFZ                       |Deutsches Rheuma-Forschungszentrum                                                     |WGL          |             6|
|Paracelsus                 |-                                                                                      |-            |             6|
|Augsburg U                 |-                                                                                      |-            |             5|
|BauA                       |Bundesanstalt für Arbeitsschutz und Arbeitsmedizin                                     |Federal      |             5|
|Berlin Institute of Health |-                                                                                      |-            |             5|
|DifE                       |Deutsches Institut für Ernährungsforschung                                             |WGL          |             5|
|DIPF                       |Deutsches Institut für Internationale Pädagogische Forschung                           |WGL          |             5|
|HZG                        |Helmholtz-Zentrum Geesthacht – Zentrum für Material- und Küstenforschung               |HGF          |             5|
|MRI                        |Max Rubner-Institut                                                                    |Federal      |             5|
|PEI                        |Paul-Ehrlich-Institut                                                                  |Federal      |             5|
|UK Koeln                   |Universitätsklinikum Köln                                                              |-            |             5|
|UniBwH                     |Helmut-Schmidt-Universität – Universität der Bundeswehr Hamburg                        |Federal      |             5|
|BGR                        |Bundesanstalt für Geowissenschaften und Rohstoffe                                      |Federal      |             4|
|FLI                        |Leibniz-Institut für Alternsforschung – Fritz-Lipmann-Institut                         |WGL          |             4|
|IZI                        |Fraunhofer-Institut für Zelltherapie und Immunologie                                   |FHG          |             4|
|mediri GmbH                |-                                                                                      |-            |             4|
|RWTH                       |Rheinisch-Westfälische Technische Hochschule Aachen                                    |-            |             4|
|Siegen U                   |-                                                                                      |-            |             4|
|TU Freiberg                |-                                                                                      |-            |             4|
|Twincore                   |-                                                                                      |-            |             4|
|UKE                        |Universitätsklinikum Hamburg-Eppendorf                                                 |-            |             4|
|Witten-Herdecke U          |-                                                                                      |-            |             4|
|ATB                        |Leibniz-Institut für Agrartechnik und Bioökonomie                                      |WGL          |             3|
|Boehringer Ingelheim       |-                                                                                      |-            |             3|
|DWD                        |Deutscher Wetterdienst                                                                 |Federal      |             3|
|Eichstätt U                |-                                                                                      |-            |             3|
|Erfurt U                   |-                                                                                      |-            |             3|
|FMP                        |Leibniz-Forschungsinstitut für Molekulare Pharmakologie                                |WGL          |             3|
|Georg-Speyer-Haus          |-                                                                                      |-            |             3|
|Hagen U                    |-                                                                                      |-            |             3|
|Hildesheim U               |-                                                                                      |-            |             3|
|IfW                        |Institut für Weltwirtschaft                                                            |WGL          |             3|
|IGPP                       |Institut für Grenzgebiete der Psychologie und Psychohygiene                            |-            |             3|
|IPB                        |Leibniz-Institut für Pflanzenbiochemie                                                 |WGL          |             3|
|Jena U                     |Friedrich-Schiller-Universität Jena                                                    |-            |             3|
|MHS Brandenburg            |Medizinische Hochschule Brandenburg Theodor Fontane                                    |-            |             3|
|Miltenyi Biotec GmbH       |-                                                                                      |-            |             3|
|MRI-TUM                    |Klinikum rechts der Isar                                                               |-            |             3|
|Parmenides Foundation      |-                                                                                      |-            |             3|
|BTU                        |Brandenburgische Technische Universität Cottbus-Senftenberg                            |-            |             2|
|DFKI                       |Deutsches Forschungszentrum für Künstliche Intelligenz                                 |-            |             2|
|DIL                        |Deutsches Institut für Lebensmitteltechnik                                             |-            |             2|
|HS Geisenheim              |-                                                                                      |-            |             2|
|IASS                       |Institute for Advanced Sustainability Studies                                          |-            |             2|
|IPK                        |Institut für Pflanzengenetik und Kulturpflanzenforschung                               |WGL          |             2|
|Passau U                   |-                                                                                      |-            |             2|
|PHB                        |Psychologische Hochschule Berlin                                                       |-            |             2|
|SGN                        |Senckenberg Gesellschaft für Naturforschung                                            |WGL          |             2|
|SRH Heidelberg             |SRH Hochschule Heidelberg                                                              |-            |             2|
|UK Dresden                 |Universitätsklinikum Carl Gustav Carus Dresden                                         |-            |             2|
|UniBwM                     |Universität der Bundeswehr München                                                     |Federal      |             2|
|Viadrina                   |-                                                                                      |-            |             2|
|BVL                        |Bundesamt für Verbraucherschutz und Lebensmittelsicherheit                             |Federal      |             1|
|GESIS                      |Leibniz-Institut für Sozialwissenschaften                                              |WGL          |             1|
|Vechta U                   |-                                                                                      |-            |             1|

Alltogether, 113 institutions with a total output of 2080 articles are currently not participating in OpenAPC.

### 2.2 Reporting coverage

Our second question focussed on reporting coverage: How large is the share of Frontiers articles our participants are reporting to OpenAPC compared to their institution's output? While we can answer this by creating a correlation between the two data sets, the following aspects should be kept in mind: 

* The matching is purely DOI-based. As a consequence, the author's affiliation may not be identical in all cases as it is not checked during the process. Entries with a diverging institution are listed in section 2.2.1.
* Research centers affiliated with the Max Planck Society (MPG) are listed in a separate table since the Max Plack Digital Libary (MPDL) does not include this kind of information when reporting Frontiers articles to OpenAPC (institution is always "MPG").


|institution          |full name                                                                 |organisation | in OpenAPC| not in OpenAPC| article count| OpenAPC coverage(%)|
|:--------------------|:-------------------------------------------------------------------------|:------------|----------:|--------------:|-------------:|-------------------:|
|Hamburg TUHH         |-                                                                         |-            |          3|              0|             3|              100.00|
|IPN                  |Leibniz-Institut für die Pädagogik der Naturwissenschaften und Mathematik |WGL          |          1|              0|             1|              100.00|
|Leibniz-Fonds        |-                                                                         |-            |          3|              0|             3|              100.00|
|Kassel U             |-                                                                         |-            |         12|              1|            13|               92.31|
|TU Chemnitz          |-                                                                         |-            |         17|              4|            21|               80.95|
|Konstanz U           |-                                                                         |-            |         72|             19|            91|               79.12|
|Bielefeld U          |-                                                                         |-            |         81|             23|           104|               77.88|
|Bremen U             |-                                                                         |-            |         28|              9|            37|               75.68|
|Bamberg U            |-                                                                         |-            |          8|              3|            11|               72.73|
|Oldenburg U          |-                                                                         |-            |         63|             24|            87|               72.41|
|FZJ - ZB             |Forschungszentrum Jülich                                                  |HGF          |         62|             24|            86|               72.09|
|KIT                  |Karlsruhe Institute of Technology                                         |-            |         30|             12|            42|               71.43|
|Goettingen U         |-                                                                         |-            |        127|             59|           186|               68.28|
|TU Muenchen          |-                                                                         |-            |         99|             49|           148|               66.89|
|Hannover U           |Gottfried Wilhelm Leibniz Universität Hannover                            |-            |          2|              1|             3|               66.67|
|Regensburg U         |-                                                                         |-            |         60|             37|            97|               61.86|
|HZDR                 |Helmholtz-Zentrum Dresden-Rossendorf                                      |HGF          |          3|              2|             5|               60.00|
|TU Ilmenau           |-                                                                         |-            |          3|              2|             5|               60.00|
|FU Berlin            |-                                                                         |-            |         76|             57|           133|               57.14|
|GFZ-Potsdam          |Helmholtz-Zentrum Potsdam – Deutsches GeoForschungsZentrum                |HGF          |          8|              6|            14|               57.14|
|Bayreuth U           |-                                                                         |-            |          5|              4|             9|               55.56|
|Leibniz-Fonds        |-                                                                         |WGL          |         21|             19|            40|               52.50|
|Potsdam U            |-                                                                         |-            |         36|             35|            71|               50.70|
|Dortmund TU          |-                                                                         |-            |          4|              4|             8|               50.00|
|MDC                  |Max-Delbrück-Centrum für Molekulare Medizin in der Helmholtz-Gemeinschaft |HGF          |          7|              7|            14|               50.00|
|Osnabrück U          |-                                                                         |-            |         15|             15|            30|               50.00|
|Ulm U                |-                                                                         |-            |         64|             72|           136|               47.06|
|TU Dresden           |-                                                                         |-            |         32|             37|            69|               46.38|
|TiHo Hannover        |-                                                                         |-            |         12|             14|            26|               46.15|
|Erlangen Nuernberg U |-                                                                         |-            |         71|             83|           154|               46.10|
|UFZ                  |Helmholtz-Zentrum für Umweltforschung – UFZ                               |HGF          |         10|             12|            22|               45.45|
|Leipzig U            |-                                                                         |-            |         27|             34|            61|               44.26|
|Stuttgart U          |-                                                                         |-            |         11|             14|            25|               44.00|
|Tuebingen U          |-                                                                         |-            |         98|            131|           229|               42.79|
|Giessen U            |-                                                                         |-            |         42|             59|           101|               41.58|
|JGU Mainz            |-                                                                         |-            |         42|             59|           101|               41.58|
|Wuerzburg U          |-                                                                         |-            |         51|             74|           125|               40.80|
|Bochum U             |-                                                                         |-            |         43|             66|           109|               39.45|
|Hannover U           |-                                                                         |-            |          7|             11|            18|               38.89|
|Heidelberg U         |-                                                                         |-            |         65|            103|           168|               38.69|
|Leibniz-IGB          |Leibniz-Institut für Gewässerökologie und Binnenfischerei                 |WGL          |          1|              2|             3|               33.33|
|Trier U              |-                                                                         |-            |          2|              4|             6|               33.33|
|Freiburg U           |-                                                                         |-            |         41|            117|           158|               25.95|
|Münster U            |-                                                                         |-            |         28|             80|           108|               25.93|
|Duisburg-Essen U     |-                                                                         |-            |         18|             54|            72|               25.00|
|TU Darmstadt         |-                                                                         |-            |          3|             14|            17|               17.65|
|TU Berlin            |-                                                                         |-            |          3|             20|            23|               13.04|
|MLU Halle-Wittenberg |-                                                                         |-            |          3|             22|            25|               12.00|
|TU Braunschweig      |-                                                                         |-            |          2|             20|            22|                9.09|
|Muenchen LMU         |-                                                                         |-            |          1|            178|           179|                0.56|
|TU Clausthal         |-                                                                         |-            |          0|              1|             1|                0.00|
|Total                |-                                                                         |-            |       1523|           1697|          3220|               47.30|

Special case: Max Planck institutes 


|institution   |full name                                                     |organisation | in OpenAPC| not in OpenAPC| article count| OpenAPC coverage(%)|
|:-------------|:-------------------------------------------------------------|:------------|----------:|--------------:|-------------:|-------------------:|
|MPI-BGC       |Max-Planck-Institut für Biogeochemie                          |MPG          |          3|              0|             3|                 100|
|MPI-biochem   |Max-Planck-Institut für Biochemie                             |MPG          |          4|              0|             4|                 100|
|MPI-EA        |Max-Planck-Institut für empirische Ästhetik                   |MPG          |         11|              0|            11|                 100|
|MPI-EB        |Max–Planck–Institut für Entwicklungsbiologie                  |MPG          |          2|              0|             2|                 100|
|MPI-HLR       |Max-Planck-Institut für Herz- und Lungenforschung             |MPG          |          5|              0|             5|                 100|
|MPI-KG        |Max-Planck-Institut für Kolloid- und Grenzflächenforschung    |MPG          |          4|              0|             4|                 100|
|MPI-MG        |Max-Planck-Institut für molekulare Genetik                    |MPG          |          2|              0|             2|                 100|
|MPI-MIS       |Max-Planck-Institut für Mathematik in den Naturwissenschaften |MPG          |          4|              0|             4|                 100|
|MPI-Muenster  |Max-Planck-Institut für molekulare Biomedizin                 |MPG          |          1|              0|             1|                 100|
|MPI-PBC       |Max-Planck-Institut für biophysikalische Chemie               |MPG          |          6|              0|             6|                 100|
|MPI-PKS       |Max-Planck-Institut für Physik komplexer Systeme              |MPG          |          1|              0|             1|                 100|
|MPI-PZ        |Max-Planck-Institut für Pflanzenzüchtungsforschung            |MPG          |         11|              0|            11|                 100|
|MPI-SF        |Max-Planck-Institut für Stoffwechselforschung                 |MPG          |          3|              0|             3|                 100|
|MPI-terMic    |Max-Planck-Institut für terrestrische Mikrobiologie           |MPG          |         12|              1|            13|                  92|
|MPI-B         |Max-Planck-Institut für Bildungsforschung                     |MPG          |         19|              2|            21|                  90|
|MPI-CBS       |Max-Planck-Institut für Kognitions- und Neurowissenschaften   |MPG          |         57|              6|            63|                  90|
|MPI-PL        |Max-Planck-Institut für Psycholinguistik                      |MPG          |         34|              4|            38|                  89|
|MPI-KYB       |Max-Planck-Institut für biologische Kybernetik                |MPG          |         15|              2|            17|                  88|
|MPI-MP        |Max-Planck-Institut für molekulare Pflanzenphysiologie        |MPG          |         15|              3|            18|                  83|
|MPI-ORN       |Max-Planck-Institut für Ornithologie                          |MPG          |          5|              1|             6|                  83|
|MPI-Psych     |Max-Planck-Institut für Psychiatrie                           |MPG          |         15|              3|            18|                  83|
|MPI-MM        |Max-Planck-Institut für Marine Mikrobiologie                  |MPG          |         14|              4|            18|                  78|
|MPI-CE        |Max-Planck-Institut für chemische Ökologie                    |MPG          |         18|              6|            24|                  75|
|MPI-evolbio   |Max-Planck-Institut für Evolutionsbiologie                    |MPG          |          3|              1|             4|                  75|
|MPI-MF        |Max-Planck-Institut für medizinische Forschung                |MPG          |          4|              2|             6|                  67|
|MPI-IB        |Max-Planck-Institut für Infektionsbiologie                    |MPG          |          6|              4|            10|                  60|
|MPI-DS        |Max-Planck-Institut für Dynamik und Selbstorganisation        |MPG          |          4|              3|             7|                  57|
|MPI-IS        |Max-Planck-Institut für Intelligente Systeme                  |MPG          |          4|              3|             7|                  57|
|MPI-EVA       |Max-Planck-Institut für evolutionäre Anthropologie            |MPG          |          5|              4|             9|                  56|
|MPI-CBG       |Max-Planck-Institut für molekulare Zellbiologie und Genetik   |MPG          |          1|              1|             2|                  50|
|MPI-EM        |Max-Planck-Institut für experimentelle Medizin                |MPG          |          2|              2|             4|                  50|
|MPI-Magdeburg |Max-Planck-Institut für Dynamik komplexer technischer Systeme |MPG          |          1|              1|             2|                  50|
|MPI-AGE       |Max-Planck-Institut für Biologie des Alterns                  |MPG          |          0|              1|             1|                   0|
|MPI-INF       |Max-Planck-Institut für Informatik                            |MPG          |          0|              1|             1|                   0|
|MPI-MPE       |Max-Planck-Institut für extraterrestrische Physik             |MPG          |          0|              1|             1|                   0|
|MPI-Neuro     |Max-Planck-Institut für Neurobiologie                         |MPG          |          0|              2|             2|                   0|
|Total         |-                                                             |-            |        291|             58|           349|                  83|

This table lists all MPG articles not reported to OpenAPC (mostly relevant to the people at MPDL only): 


|institution   |full name                                                     |DOI                      |Journal                                   |
|:-------------|:-------------------------------------------------------------|:------------------------|:-----------------------------------------|
|MPI-AGE       |Max-Planck-Institut für Biologie des Alterns                  |10.3389/fmicb.2018.00251 |Frontiers in Microbiology                 |
|MPI-B         |Max-Planck-Institut für Bildungsforschung                     |10.3389/fpsyg.2018.00294 |Frontiers in Psychology                   |
|MPI-B         |Max-Planck-Institut für Bildungsforschung                     |10.3389/frobt.2018.00082 |Frontiers in Robotics and AI              |
|MPI-CBG       |Max-Planck-Institut für molekulare Zellbiologie und Genetik   |10.3389/fnins.2017.00742 |Frontiers in Neuroscience                 |
|MPI-CBS       |Max-Planck-Institut für Kognitions- und Neurowissenschaften   |10.3389/fnagi.2018.00011 |Frontiers in Aging Neuroscience           |
|MPI-CBS       |Max-Planck-Institut für Kognitions- und Neurowissenschaften   |10.3389/fpsyg.2018.00036 |Frontiers in Psychology                   |
|MPI-CBS       |Max-Planck-Institut für Kognitions- und Neurowissenschaften   |10.3389/fpsyg.2018.00570 |Frontiers in Psychology                   |
|MPI-CBS       |Max-Planck-Institut für Kognitions- und Neurowissenschaften   |10.3389/fnhum.2018.00239 |Frontiers in Human Neuroscience           |
|MPI-CBS       |Max-Planck-Institut für Kognitions- und Neurowissenschaften   |10.3389/fnhum.2018.00240 |Frontiers in Human Neuroscience           |
|MPI-CBS       |Max-Planck-Institut für Kognitions- und Neurowissenschaften   |10.3389/fnhum.2018.00244 |Frontiers in Human Neuroscience           |
|MPI-CE        |Max-Planck-Institut für chemische Ökologie                    |10.3389/fncel.2014.00448 |Frontiers in Cellular Neuroscience        |
|MPI-CE        |Max-Planck-Institut für chemische Ökologie                    |10.3389/fevo.2018.00019  |Frontiers in Ecology and Evolution        |
|MPI-CE        |Max-Planck-Institut für chemische Ökologie                    |10.3389/fphys.2018.00049 |Frontiers in Physiology                   |
|MPI-CE        |Max-Planck-Institut für chemische Ökologie                    |10.3389/fncel.2018.00094 |Frontiers in Cellular Neuroscience        |
|MPI-CE        |Max-Planck-Institut für chemische Ökologie                    |10.3389/fncel.2018.00186 |Frontiers in Cellular Neuroscience        |
|MPI-CE        |Max-Planck-Institut für chemische Ökologie                    |10.3389/fpls.2018.00787  |Frontiers in Plant Science                |
|MPI-DS        |Max-Planck-Institut für Dynamik und Selbstorganisation        |10.3389/fphy.2014.00018  |Frontiers in Physics                      |
|MPI-DS        |Max-Planck-Institut für Dynamik und Selbstorganisation        |10.3389/fphy.2018.00008  |Frontiers in Physics                      |
|MPI-DS        |Max-Planck-Institut für Dynamik und Selbstorganisation        |10.3389/fphy.2018.00039  |Frontiers in Physics                      |
|MPI-EM        |Max-Planck-Institut für experimentelle Medizin                |10.3389/fncel.2018.00117 |Frontiers in Cellular Neuroscience        |
|MPI-EM        |Max-Planck-Institut für experimentelle Medizin                |10.3389/fnins.2018.00467 |Frontiers in Neuroscience                 |
|MPI-EVA       |Max-Planck-Institut für evolutionäre Anthropologie            |10.3389/fpsyg.2018.00250 |Frontiers in Psychology                   |
|MPI-EVA       |Max-Planck-Institut für evolutionäre Anthropologie            |10.3389/fevo.2018.00060  |Frontiers in Ecology and Evolution        |
|MPI-EVA       |Max-Planck-Institut für evolutionäre Anthropologie            |10.3389/fnins.2018.00315 |Frontiers in Neuroscience                 |
|MPI-EVA       |Max-Planck-Institut für evolutionäre Anthropologie            |10.3389/fevo.2018.00085  |Frontiers in Ecology and Evolution        |
|MPI-evolbio   |Max-Planck-Institut für Evolutionsbiologie                    |10.3389/fevo.2014.00017  |Frontiers in Ecology and Evolution        |
|MPI-IB        |Max-Planck-Institut für Infektionsbiologie                    |10.3389/fimmu.2014.00036 |Frontiers in Immunology                   |
|MPI-IB        |Max-Planck-Institut für Infektionsbiologie                    |10.3389/fimmu.2017.01744 |Frontiers in Immunology                   |
|MPI-IB        |Max-Planck-Institut für Infektionsbiologie                    |10.3389/fimmu.2018.00121 |Frontiers in Immunology                   |
|MPI-IB        |Max-Planck-Institut für Infektionsbiologie                    |10.3389/fimmu.2018.01346 |Frontiers in Immunology                   |
|MPI-INF       |Max-Planck-Institut für Informatik                            |10.3389/fnhum.2018.00105 |Frontiers in Human Neuroscience           |
|MPI-IS        |Max-Planck-Institut für Intelligente Systeme                  |10.3389/fmech.2015.00006 |Frontiers in Mechanical Engineering       |
|MPI-IS        |Max-Planck-Institut für Intelligente Systeme                  |10.3389/fnbot.2017.00008 |Frontiers in Neurorobotics                |
|MPI-IS        |Max-Planck-Institut für Intelligente Systeme                  |10.3389/frobt.2018.00067 |Frontiers in Robotics and AI              |
|MPI-KYB       |Max-Planck-Institut für biologische Kybernetik                |10.3389/fnsys.2014.00044 |Frontiers in Systems Neuroscience         |
|MPI-KYB       |Max-Planck-Institut für biologische Kybernetik                |10.3389/fncom.2016.00066 |Frontiers in Computational Neuroscience   |
|MPI-Magdeburg |Max-Planck-Institut für Dynamik komplexer technischer Systeme |10.3389/fchem.2018.00164 |Frontiers in Chemistry                    |
|MPI-MF        |Max-Planck-Institut für medizinische Forschung                |10.3389/fnmol.2017.00214 |Frontiers in Molecular Neuroscience       |
|MPI-MF        |Max-Planck-Institut für medizinische Forschung                |10.3389/fnmol.2018.00199 |Frontiers in Molecular Neuroscience       |
|MPI-MM        |Max-Planck-Institut für Marine Mikrobiologie                  |10.3389/fmicb.2014.00231 |Frontiers in Microbiology                 |
|MPI-MM        |Max-Planck-Institut für Marine Mikrobiologie                  |10.3389/fmicb.2016.01011 |Frontiers in Microbiology                 |
|MPI-MM        |Max-Planck-Institut für Marine Mikrobiologie                  |10.3389/fmicb.2018.00858 |Frontiers in Microbiology                 |
|MPI-MM        |Max-Planck-Institut für Marine Mikrobiologie                  |10.3389/fmicb.2018.00680 |Frontiers in Microbiology                 |
|MPI-MP        |Max-Planck-Institut für molekulare Pflanzenphysiologie        |10.3389/fmolb.2015.00051 |Frontiers in Molecular Biosciences        |
|MPI-MP        |Max-Planck-Institut für molekulare Pflanzenphysiologie        |10.3389/fpls.2017.02152  |Frontiers in Plant Science                |
|MPI-MP        |Max-Planck-Institut für molekulare Pflanzenphysiologie        |10.3389/fpls.2018.00538  |Frontiers in Plant Science                |
|MPI-MPE       |Max-Planck-Institut für extraterrestrische Physik             |10.3389/fspas.2017.00067 |Frontiers in Astronomy and Space Sciences |
|MPI-Neuro     |Max-Planck-Institut für Neurobiologie                         |10.3389/fnana.2016.00062 |Frontiers in Neuroanatomy                 |
|MPI-Neuro     |Max-Planck-Institut für Neurobiologie                         |10.3389/fncir.2018.00039 |Frontiers in Neural Circuits              |
|MPI-ORN       |Max-Planck-Institut für Ornithologie                          |10.3389/fendo.2018.00063 |Frontiers in Endocrinology                |
|MPI-PL        |Max-Planck-Institut für Psycholinguistik                      |10.3389/fpsyg.2016.00222 |Frontiers in Psychology                   |
|MPI-PL        |Max-Planck-Institut für Psycholinguistik                      |10.3389/fnhum.2018.00034 |Frontiers in Human Neuroscience           |
|MPI-PL        |Max-Planck-Institut für Psycholinguistik                      |10.3389/fnins.2018.00020 |Frontiers in Neuroscience                 |
|MPI-PL        |Max-Planck-Institut für Psycholinguistik                      |10.3389/fnmol.2018.00047 |Frontiers in Molecular Neuroscience       |
|MPI-Psych     |Max-Planck-Institut für Psychiatrie                           |10.3389/fnbeh.2014.00452 |Frontiers in Behavioral Neuroscience      |
|MPI-Psych     |Max-Planck-Institut für Psychiatrie                           |10.3389/fncir.2018.00036 |Frontiers in Neural Circuits              |
|MPI-Psych     |Max-Planck-Institut für Psychiatrie                           |10.3389/fncir.2018.00042 |Frontiers in Neural Circuits              |
|MPI-terMic    |Max-Planck-Institut für terrestrische Mikrobiologie           |10.3389/fmicb.2018.00401 |Frontiers in Microbiology                 |



#### 2.2.1 Affiliation mismatches

Out of 1839 matches between the Frontiers and the OpenAPC data sets, 36 cases of mismatched affiliations occured:


|DOI                      |organisation (Frontiers) |institution (Frontiers) |institution (OpenAPC)       |
|:------------------------|:------------------------|:-----------------------|:---------------------------|
|10.3389/fphy.2014.00002  |-                        |Hamburg U               |FZJ - ZB                    |
|10.3389/fpsyg.2014.00686 |-                        |RWTH                    |FZJ - ZB                    |
|10.3389/fmicb.2014.00459 |WGL                      |Leibniz-IGB             |MPG                         |
|10.3389/fnins.2014.00413 |WGL                      |IfADo                   |Dortmund TU                 |
|10.3389/fnins.2014.00352 |-                        |RWTH                    |FWF - Austrian Science Fund |
|10.3389/fpsyg.2015.01326 |MPG                      |MPI-PL                  |OpenAIRE                    |
|10.3389/fmolb.2015.00055 |-                        |Duesseldorf U           |FZJ - ZB                    |
|10.3389/fpsyg.2015.01084 |-                        |Osnabrück U             |OpenAIRE                    |
|10.3389/fmars.2015.00113 |WGL                      |ZMT                     |Bremen U                    |
|10.3389/fnins.2015.00341 |WGL                      |IfADo                   |Dortmund TU                 |
|10.3389/fnhum.2016.00044 |-                        |RWTH                    |FWF - Austrian Science Fund |
|10.3389/fnhum.2016.00291 |-                        |DFKI                    |Bremen U                    |
|10.3389/fpsyg.2016.00483 |-                        |Heidelberg U            |OpenAIRE                    |
|10.3389/fpls.2016.00669  |WGL                      |ZALF                    |Leibniz-Fonds               |
|10.3389/fpsyg.2017.00239 |-                        |Jena U                  |MPG                         |
|10.3389/fmicb.2016.01446 |HGF                      |FZJ - ZB                |UFZ                         |
|10.3389/fpsyt.2017.00029 |WGL                      |Leibniz-Fonds           |Dortmund TU                 |
|10.3389/fpsyg.2016.01939 |WGL                      |IfADo                   |Dortmund TU                 |
|10.3389/fpsyg.2016.00865 |-                        |Oldenburg U             |Erlangen Nuernberg U        |
|10.3389/fncel.2017.00009 |-                        |Osnabrück U             |MPG                         |
|10.3389/fncel.2016.00292 |HGF                      |DZNE                    |Goettingen U                |
|10.3389/fonc.2015.00258  |HGF                      |GSI                     |OpenAIRE                    |
|10.3389/fonc.2015.00258  |-                        |Muenchen LMU            |OpenAIRE                    |
|10.3389/fonc.2015.00258  |-                        |mediri GmbH             |OpenAIRE                    |
|10.3389/fonc.2015.00258  |-                        |mediri GmbH             |OpenAIRE                    |
|10.3389/fonc.2015.00258  |-                        |mediri GmbH             |OpenAIRE                    |
|10.3389/fncel.2017.00115 |WGL                      |LIN                     |Leibniz-Fonds               |
|10.3389/fnhum.2017.00290 |WGL                      |Leibniz-Fonds           |Dortmund TU                 |
|10.3389/fonc.2015.00258  |-                        |mediri GmbH             |OpenAIRE                    |
|10.3389/fgene.2015.00296 |-                        |IMB                     |JGU Mainz                   |
|10.3389/fpsyg.2017.01584 |-                        |Lübeck U                |MPG                         |
|10.3389/fpsyg.2014.01472 |WGL                      |SGN                     |Tuebingen U                 |
|10.3389/fnbeh.2014.00385 |-                        |Frankfurt U             |Tuebingen U                 |
|10.3389/fncir.2014.00093 |-                        |Freiburg U              |Tuebingen U                 |
|10.3389/fncir.2014.00050 |-                        |Hamburg U               |FZJ - ZB                    |
|10.3389/fnbeh.2014.00212 |-                        |ZI                      |Heidelberg U                |

### 2.3 Calculated costs for non-participants



In our last evaluation we tried to estimate APC costs for non-participants based on the Frontiers article list and OpenAPC mean values for the according journals. 
As a first step, the Frontiers article list was enriched with crossref metadata using our standard OpenAPC tools. This was necessary to create journal titles compatible with the ones used in OpenAPC (which are all crossref-based).  
This data could then be used to generate a list of all journals in question:



|journal                                          | article count|
|:------------------------------------------------|-------------:|
|Frontiers in Psychology                          |           304|
|Frontiers in Microbiology                        |           224|
|Frontiers in Immunology                          |           222|
|Frontiers in Plant Science                       |           184|
|Frontiers in Human Neuroscience                  |           113|
|Frontiers in Physiology                          |            88|
|Frontiers in Marine Science                      |            75|
|Frontiers in Neuroscience                        |            68|
|Frontiers in Cellular Neuroscience               |            58|
|Frontiers in Behavioral Neuroscience             |            57|
|Frontiers in Neurology                           |            57|
|Frontiers in Pharmacology                        |            50|
|Frontiers in Cellular and Infection Microbiology |            40|
|Frontiers in Aging Neuroscience                  |            38|
|Frontiers in Oncology                            |            36|
|Frontiers in Psychiatry                          |            36|
|Frontiers in Genetics                            |            34|
|Frontiers in Molecular Neuroscience              |            32|
|Frontiers in Endocrinology                       |            30|
|Frontiers in Surgery                             |            27|
|Frontiers in Pediatrics                          |            25|
|Frontiers in Ecology and Evolution               |            22|
|Frontiers in Neuroanatomy                        |            22|
|Frontiers in Medicine                            |            19|
|Frontiers in Public Health                       |            16|
|Frontiers in Computational Neuroscience          |            15|
|Frontiers in Bioengineering and Biotechnology    |            13|
|Frontiers in Cardiovascular Medicine             |            13|
|Frontiers in Neuroinformatics                    |            13|
|Frontiers in Neurorobotics                       |            13|
|Frontiers in Cell and Developmental Biology      |            12|
|Frontiers in Robotics and AI                     |            12|
|Frontiers in Systems Neuroscience                |            12|
|Frontiers in Earth Science                       |            11|
|Frontiers in Materials                           |            10|
|Frontiers in Synaptic Neuroscience               |            10|
|Frontiers in Molecular Biosciences               |             9|
|Frontiers in Neural Circuits                     |             9|
|Frontiers in Chemistry                           |             8|
|Frontiers in Environmental Science               |             8|
|Frontiers in Integrative Neuroscience            |             7|
|Frontiers in Physics                             |             5|
|Frontiers in Veterinary Science                  |             5|
|Frontiers in Applied Mathematics and Statistics  |             4|
|Frontiers in Communication                       |             3|
|Frontiers in ICT                                 |             3|
|Frontiers in Energy Research                     |             2|
|Frontiers in Nutrition                           |             2|
|Frontiers in Built Environment                   |             1|
|Frontiers in Digital Humanities                  |             1|
|Frontiers in Sociology                           |             1|
|Frontiers in Sustainable Food Systems            |             1|

As we can see, the 2080 articles already calculated before originate from 52 different journals.
Next, these journals were looked up in OpenAPC to calculate mean costs from all associated articles:


|journal                                          | article count| OpenAPC article count| OpenAPC mean costs|
|:------------------------------------------------|-------------:|---------------------:|------------------:|
|Frontiers in Psychology                          |           304|                   849|               1501|
|Frontiers in Microbiology                        |           224|                   327|               1603|
|Frontiers in Immunology                          |           222|                   188|               1523|
|Frontiers in Plant Science                       |           184|                   301|               1434|
|Frontiers in Human Neuroscience                  |           113|                   371|               1431|
|Frontiers in Physiology                          |            88|                   136|               1472|
|Frontiers in Marine Science                      |            75|                    31|               1612|
|Frontiers in Neuroscience                        |            68|                   145|               1526|
|Frontiers in Cellular Neuroscience               |            58|                   112|               1420|
|Frontiers in Behavioral Neuroscience             |            57|                   128|               1563|
|Frontiers in Neurology                           |            57|                    67|               1420|
|Frontiers in Pharmacology                        |            50|                    43|               1380|
|Frontiers in Cellular and Infection Microbiology |            40|                    48|               1433|
|Frontiers in Aging Neuroscience                  |            38|                    65|               1633|
|Frontiers in Oncology                            |            36|                    49|               1389|
|Frontiers in Psychiatry                          |            36|                    62|               1449|
|Frontiers in Genetics                            |            34|                    70|               1154|
|Frontiers in Molecular Neuroscience              |            32|                    48|               1566|
|Frontiers in Endocrinology                       |            30|                    36|               1171|
|Frontiers in Surgery                             |            27|                     7|               1378|
|Frontiers in Pediatrics                          |            25|                    12|               1297|
|Frontiers in Ecology and Evolution               |            22|                    44|               1359|
|Frontiers in Neuroanatomy                        |            22|                    42|               1559|
|Frontiers in Medicine                            |            19|                     7|               1490|
|Frontiers in Public Health                       |            16|                    19|               1428|
|Frontiers in Computational Neuroscience          |            15|                    69|               1530|
|Frontiers in Bioengineering and Biotechnology    |            13|                    40|               1251|
|Frontiers in Cardiovascular Medicine             |            13|                     0|                 NA|
|Frontiers in Neuroinformatics                    |            13|                    21|               1538|
|Frontiers in Neurorobotics                       |            13|                    12|               1657|
|Frontiers in Cell and Developmental Biology      |            12|                    27|               1409|
|Frontiers in Robotics and AI                     |            12|                    13|               1233|
|Frontiers in Systems Neuroscience                |            12|                    49|               1234|
|Frontiers in Earth Science                       |            11|                    22|               1484|
|Frontiers in Materials                           |            10|                     7|               1355|
|Frontiers in Synaptic Neuroscience               |            10|                     9|               1494|
|Frontiers in Molecular Biosciences               |             9|                    16|               1336|
|Frontiers in Neural Circuits                     |             9|                    45|               1424|
|Frontiers in Chemistry                           |             8|                    17|               1501|
|Frontiers in Environmental Science               |             8|                    13|               1540|
|Frontiers in Integrative Neuroscience            |             7|                    24|               1316|
|Frontiers in Physics                             |             5|                    21|               1284|
|Frontiers in Veterinary Science                  |             5|                    16|               1379|
|Frontiers in Applied Mathematics and Statistics  |             4|                     3|               1178|
|Frontiers in Communication                       |             3|                     1|                738|
|Frontiers in ICT                                 |             3|                     5|               1163|
|Frontiers in Energy Research                     |             2|                     6|               1653|
|Frontiers in Nutrition                           |             2|                     5|                972|
|Frontiers in Built Environment                   |             1|                     2|               1211|
|Frontiers in Digital Humanities                  |             1|                     1|               1010|
|Frontiers in Sociology                           |             1|                     0|                 NA|
|Frontiers in Sustainable Food Systems            |             1|                     0|                 NA|

A small problem is that not all journals occur in OpenAPC, so we lack cost information for some articles. 
We mitigated this by using official APC prices as stated by Frontiers on their [web site](https://www.frontiersin.org/about/publishing-fees) ("A type" articles, USD values were converted to Euro using the average exchange rate from 2014 to 2018):



|Journal                               | official APC ('A type' article)|
|:-------------------------------------|-------------------------------:|
|Frontiers in Cardiovascular Medicine  |                            1620|
|Frontiers in Sociology                |                             810|
|Frontiers in Sustainable Food Systems |                             810|

This data could now be used to estimate APC costs for Frontiers articles from 2014 to 2018 for non-participating institutions. As an example, here's are full caluclation for the 116 articles published at the University of Hamburg ("Hamburg U"):



|journal                                          | article count| OpenAPC mean costs| total costs|
|:------------------------------------------------|-------------:|------------------:|-----------:|
|Frontiers in Psychology                          |            21|               1501|       31521|
|Frontiers in Immunology                          |            11|               1523|       16750|
|Frontiers in Microbiology                        |             8|               1603|       12827|
|Frontiers in Aging Neuroscience                  |             7|               1633|       11431|
|Frontiers in Plant Science                       |             7|               1434|       10039|
|Frontiers in Cellular Neuroscience               |             6|               1420|        8519|
|Frontiers in Human Neuroscience                  |             6|               1431|        8585|
|Frontiers in Neurology                           |             6|               1420|        8522|
|Frontiers in Cardiovascular Medicine             |             4|               1620|        6480|
|Frontiers in Neuroscience                        |             4|               1526|        6105|
|Frontiers in Oncology                            |             4|               1389|        5558|
|Frontiers in Behavioral Neuroscience             |             3|               1563|        4689|
|Frontiers in Medicine                            |             3|               1490|        4469|
|Frontiers in Pharmacology                        |             3|               1380|        4141|
|Frontiers in Endocrinology                       |             2|               1171|        2342|
|Frontiers in Marine Science                      |             2|               1612|        3223|
|Frontiers in Neuroanatomy                        |             2|               1559|        3118|
|Frontiers in Neurorobotics                       |             2|               1657|        3315|
|Frontiers in Physiology                          |             2|               1472|        2943|
|Frontiers in Psychiatry                          |             2|               1449|        2899|
|Frontiers in Systems Neuroscience                |             2|               1234|        2468|
|Frontiers in Cellular and Infection Microbiology |             1|               1433|        1433|
|Frontiers in Chemistry                           |             1|               1501|        1501|
|Frontiers in Ecology and Evolution               |             1|               1359|        1359|
|Frontiers in Integrative Neuroscience            |             1|               1316|        1316|
|Frontiers in Molecular Neuroscience              |             1|               1566|        1566|
|Frontiers in Neural Circuits                     |             1|               1424|        1424|
|Frontiers in Neuroinformatics                    |             1|               1538|        1538|
|Frontiers in Physics                             |             1|               1284|        1284|
|Frontiers in Surgery                             |             1|               1378|        1378|
|Total                                            |           116|                 NA|      172744|

Finally, a table of estimated costs for all non-participants using the same approach:


|institution                | article count| estimated costs (€)|
|:--------------------------|-------------:|-------------------:|
|Charité                    |           156|              227022|
|Hamburg U                  |           116|              172744|
|Frankfurt U                |           108|              159191|
|Jena U                     |            86|              128013|
|MH Hannover                |            85|              124149|
|Marburg U                  |            77|              114691|
|RWTH                       |            77|              113191|
|Duesseldorf U              |            69|               99864|
|Koeln U                    |            65|               92879|
|Bonn U                     |            60|               87158|
|Magdeburg U                |            57|               83442|
|Kiel U                     |            56|               84478|
|Saarland U                 |            54|               79930|
|Lübeck U                   |            47|               68482|
|HU Berlin                  |            45|               64941|
|Rostock U                  |            44|               67168|
|IPK                        |            39|               55248|
|HZI                        |            36|               54153|
|Hohenheim U                |            35|               50339|
|DKFZ                       |            34|               48777|
|DZNE                       |            31|               46751|
|HMGU                       |            31|               46272|
|GEOMAR                     |            28|               44939|
|Greifswald U               |            28|               41819|
|DSHS                       |            26|               38479|
|IWM                        |            24|               34961|
|HKI                        |            23|               35435|
|ZMT                        |            23|               35793|
|IGZ                        |            21|               30287|
|TU Kaiserslautern          |            21|               30726|
|AWI                        |            20|               31843|
|DLR                        |            18|               28319|
|IOW                        |            18|               28618|
|LIN                        |            18|               26528|
|RKI                        |            17|               25473|
|Bremen Jacobs              |            15|               22781|
|JKI                        |            13|               19233|
|DSMZ                       |            12|               19241|
|FBN                        |            12|               16366|
|FLI                        |            11|               16155|
|HMTMH                      |            11|               16235|
|IfADo                      |            11|               16600|
|Lüneburg U                 |            10|               14446|
|ZI                         |            10|               15044|
|FZB                        |             9|               13671|
|Koblenz U                  |             9|               13548|
|Paderborn U                |             9|               13095|
|UKSH                       |             9|               12586|
|ZALF                       |             9|               13959|
|BfR                        |             8|               12432|
|FIAS                       |             8|               11668|
|GSI                        |             8|               11382|
|IME                        |             8|               11758|
|Mannheim U                 |             8|               11938|
|Vivantes                   |             8|               11023|
|Bayer AG                   |             7|               10057|
|IMB                        |             7|                8547|
|DPZ                        |             6|                8971|
|DRFZ                       |             6|                9136|
|Paracelsus                 |             6|                7986|
|Augsburg U                 |             5|                6753|
|BauA                       |             5|                7446|
|Berlin Institute of Health |             5|                7667|
|DifE                       |             5|                6845|
|DIPF                       |             5|                7505|
|HZG                        |             5|                7805|
|MRI                        |             5|                8017|
|PEI                        |             5|                7480|
|UK Koeln                   |             5|                6658|
|UniBwH                     |             5|                7145|
|BGR                        |             4|                6294|
|IZI                        |             4|                6186|
|mediri GmbH                |             4|                5558|
|Siegen U                   |             4|                5853|
|TU Freiberg                |             4|                6414|
|Twincore                   |             4|                6172|
|UKE                        |             4|                5773|
|Witten-Herdecke U          |             4|                5893|
|ATB                        |             3|                4810|
|Boehringer Ingelheim       |             3|                4360|
|DWD                        |             3|                4146|
|Eichstätt U                |             3|                4433|
|Erfurt U                   |             3|                3812|
|FMP                        |             3|                4000|
|Georg-Speyer-Haus          |             3|                4435|
|Hagen U                    |             3|                4503|
|Hildesheim U               |             3|                4503|
|IfW                        |             3|                4627|
|IGPP                       |             3|                4659|
|IPB                        |             3|                4303|
|MHS Brandenburg            |             3|                4400|
|Miltenyi Biotec GmbH       |             3|                4568|
|MRI-TUM                    |             3|                4163|
|Parmenides Foundation      |             3|                4503|
|BTU                        |             2|                2864|
|DFKI                       |             2|                2664|
|DIL                        |             2|                2576|
|HS Geisenheim              |             2|                2868|
|IASS                       |             2|                2971|
|Passau U                   |             2|                2664|
|PHB                        |             2|                3002|
|SGN                        |             2|                2860|
|SRH Heidelberg             |             2|                3027|
|UK Dresden                 |             2|                2998|
|UniBwM                     |             2|                3002|
|Viadrina                   |             2|                3002|
|BVL                        |             1|                1251|
|GESIS                      |             1|                1010|
|Vechta U                   |             1|                1633|
|Total                      |          2080|             3070041|

